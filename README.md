DESCRIPTION:

The following project is written entirely in "Golang"
The data sample is used from this repository : https://github.com/jdorfman/awesome-jsondatasets#tv-shows
The code takes a certain show from the provided list as its data sample, in order to do some operations such as:
            1- Counting the number of episodes per season in that show( the parameter season should be added in the URL request)
            2- Identifying the title of an episode of a show given the season and the number of that episode
Changing the show from which the data is token is quite easy by changing the variable "tvshow" in the code with another show provided in the comments of the code.

EXAMPLE:

After running the code on the cmd or git Bash using the command "go run tv_show.go", we can type in the following URL on the browser "http://localhost:10000/episodes/count/2" : it would return 12 which is the number of episodes of the season 2.
NB : The show selected in the variable "tvshow" is mr-robot ( it is changeable). 

To check the other endpoint, we could give as an URL request: "http://localhost:10000/episodes/title/1/1", it would return the following episode title (of the first episode of the first season from the show mr-robot): "eps1.0_hellofriend.mov"

NB: To parse the Json file (outside the code for checking purposes), we can use the web service : https://jsonformatter.curiousconcept.com/#




