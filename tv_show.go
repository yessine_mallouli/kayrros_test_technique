package main

import (
    "fmt"
    "log"
    "net/http"
    "encoding/json"
   	"github.com/gorilla/mux"
	"io/ioutil"
	"strconv"
	"net/url"
)
type Show struct {
	Id int64 `json:"id"`
	Url string `json:"url"`
	Name string `json:"name"`
	Type_1 string `json:"type"`
	Language string `json:"English"`
	Genres []string `json:"genres"`
	Status string `json:"Ended"`
	Runtime int64 `json:"runtime"`
	Premiered string `json:"premiered"`
	OfficialSite string `json:"officialSite"`
	Schedule Sched `json:"schedule"`
	Rating Rate `json:"rating"`
	Weight float64 `json:"weight"`
	Network NetW `json:"network"`
	WebChannel string `json:"webChannel"`
	Externals Ext `json:"externals"`
	Image Im `json:"image"`
	Summary string `json:"summary"`
	Updated int64 `json:"updated"`
	Links Link `json:"_links"`
	Embedded Episodes `json:"_embedded"`
}
type Link struct{
	Self Ref `json:"self"`
	Previousepisode Ref `json:"previousepisode"`
}
type Ref struct{
	Href string `json:"href"`
}
type Im struct{
	Medium string `json:"medium"`
	Original string `json:"original"`
}
type Ext struct{
	Tvrage int64 `json:"tvrage"`
	Thetvdb int64 `json:"thetvdb"`
	Imdb string `json:"imdb"`
}
type NetW struct {
	Id int64 `json:"id"`
	Name string `json:"name"`
	Country Countr_y `json:"country"`
}
type Countr_y struct{
	Name string `json:"name"`
	Code string `json:"code"`
	Timezone string `json:"timezone"`
}
type Sched struct {
	Time string `json:"time"`
	Days []string `json:"days"`
}
type Rate struct {
	Average float64 `json:"average"`
}

type Episode struct {
	Id int `json:"id"`
	Url string `json:"url"`
	Name string `json:"name"`
	Season int	`json:"season"`
	Number int	`json:"number"`
	Type_2 string `json:"type"`
	Airdate string `json:"airdate"`
	Airtime string `json:"airtime"`
	Airstamp string `json:"airstamp"`
	Image Im `json:"image"`
	Summary string `json:"summary"`
	Links Link `json:"_links"`

}
type Episodes struct {
	Episodes []Episode `json:"episodes"`
}
func returnAllEpisodes(w http.ResponseWriter, r *http.Request){
	w.Header().Set("Content-Type","application/json")
    fmt.Println("Endpoint Hit: returnAllEpisodes")
    json.NewEncoder(w).Encode(episodes)
}
func countEpisodes(w http.ResponseWriter, r *http.Request){
	w.Header().Set("Content-Type","application/json")
    fmt.Println("Endpoint Hit: countEpisodes")
	var counter int = 0
	vars := mux.Vars(r)
	season := vars["season"] 		
	if _, err := strconv.Atoi(season); err == nil {
		fmt.Printf("%q looks like a number.\n", season)
		for _, episode := range episodes {
			if strconv.Itoa(episode.Season) == season {
				counter = counter + 1
			}
		}
	}else{
		var error_message string = "season value ["+season + "] is not a number" 
		json.NewEncoder(w).Encode(error_message)
	}   
    json.NewEncoder(w).Encode(counter)
}
func returnEpisodeTitle(w http.ResponseWriter, r *http.Request){
	w.Header().Set("Content-Type","application/json")
    fmt.Println("Endpoint Hit: returnEpisodeTitle")
	vars := mux.Vars(r)
	season := vars["season"]
	number := vars["number"]
	if _, err := strconv.Atoi(season); err == nil {
		fmt.Printf("%q looks like a number.\n", season)
		if _, err := strconv.Atoi(number); err == nil {
			fmt.Printf("%q looks like a number.\n", number)
			for _, episode := range episodes {
				if strconv.Itoa(episode.Season) == season && strconv.Itoa(episode.Number) == number  {
					json.NewEncoder(w).Encode(episode.Name)
				}
			}
		}else{
			var error_message string = "episode number value ["+number+ "] is not a number" 
			json.NewEncoder(w).Encode(error_message)
		}	
	}else{
		var error_message string = "season value ["+season + "] is not a number" 
		json.NewEncoder(w).Encode(error_message)
	} 
}

func returnSingleEpisode(w http.ResponseWriter, r *http.Request){
	w.Header().Set("Content-Type","application/json")
	fmt.Println("Endpoint Hit: returnSingleEpisode")
    vars := mux.Vars(r)
    key := vars["id"]
	for _, episode := range episodes {
        if strconv.Itoa(episode.Id) == key {
            json.NewEncoder(w).Encode(episode)
        }	
    }
}

func handleRequests() {
    // creates a new instance of a mux router
    myRouter := mux.NewRouter().StrictSlash(true)
	myRouter.HandleFunc("/episodes", returnAllEpisodes)
	myRouter.HandleFunc("/episodes/count/{season}", countEpisodes)
	myRouter.HandleFunc("/episodes/{id}", returnSingleEpisode)
	myRouter.HandleFunc("/episodes/title/{season}/{number}", returnEpisodeTitle)
    log.Fatal(http.ListenAndServe(":10000", myRouter))
}

var episodes []Episode
func main() {

	//available tv_shows=[mr-robot,better-call-saul,Homeland,silicon-valley,the-walking-dead,south-park,game-of-thrones]
	//[house-of-cards,big-bang-theory,narcos,black-mirror,stranger-things,rick-&-morty,westworld]
	tvshow := "mr-robot"
	FromTvshow := url.QueryEscape(tvshow)

	url := fmt.Sprintf("http://api.tvmaze.com/singlesearch/shows?q=%s&embed=episodes", FromTvshow)
    
	// Build the request
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		log.Fatal("NewRequest: ", err)
		return
	}
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		log.Fatal("Do: ", err)
		return
	}
	defer resp.Body.Close()
	var show Show
	
	body, readErr := ioutil.ReadAll(resp.Body)
	if readErr != nil {
		log.Fatal(readErr)
	}
	jsonErr := json.Unmarshal(body, &show)
	if jsonErr != nil {
		log.Fatal(jsonErr)
	}
	fmt.Println("Response status:", resp.Status)
	
	episodes = show.Embedded.Episodes
	//fmt.Println("episodes = ",episodes)
	
	handleRequests()
    
}